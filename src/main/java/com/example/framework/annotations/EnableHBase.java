package com.example.framework.annotations;

import com.example.framework.core.HBaseModuleConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author Eduardo Franceschi
 * @since 2019-04-11.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import(HBaseModuleConfiguration.class)
@Configuration
public @interface EnableHBase {
}
