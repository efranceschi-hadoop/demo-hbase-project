package com.example.framework.core;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Eduardo Franceschi
 * @since 2019-04-11.
 */
public class HBaseTemplate<T> {

	private final Configuration configuration;
	private final HBaseHelper<T> helper;

	public HBaseTemplate(Class<T> tClass, Configuration configuration) {
		this.configuration = configuration;
		this.helper = new HBaseHelper<>(tClass);
	}

	public byte[] extractRowKey(T obj) {
		return helper.extractRowKey(obj);
	}

	public T save(T obj) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Put put = helper.buildPut(obj);
			getTable(connection).put(put);
			return obj;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public T findByRowKey(T obj) {
		return findByRowKey(extractRowKey(obj));
	}

	public T findByRowKey(byte[] rowkey) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Get get = helper.buildGet(rowkey);
			Result result = getTable(connection).get(get);
			return helper.resultToObject(result);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public List<T> scan() {
		return scan(new Scan());
	}

	public List<T> scan(String rowPrefix) {
		Scan scan = new Scan();
		scan.setRowPrefixFilter(Bytes.toBytes(rowPrefix));
		return scan(scan);
	}

	public List<T> scan(String startRow, String stopRow) {
		return scan(Bytes.toBytes(startRow), Bytes.toBytes(stopRow));
	}

	public List<T> scan(byte[] rowPrefix) {
		return scan(new Scan(rowPrefix));
	}

	public List<T> scan(byte[] startRow, byte[] stopRow) {
		Scan scan = new Scan();
		scan.setStartRow(startRow);
		scan.setStopRow(stopRow);
		return scan(scan);
	}

	public List<T> scan(Scan scan) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			ResultScanner resultScanner = getTable(connection).getScanner(scan);
			Iterator<Result> it = resultScanner.iterator();
			ArrayList<T> result = new ArrayList<>();
			while (it.hasNext()) {
				result.add(helper.resultToObject(it.next()));
			}
			return result;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private Table getTable(Connection connection) throws IOException {
		return connection.getTable(TableName.valueOf(helper.getTableName()));
	}

	public boolean isTableExists() {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			return helper.isTableExists(connection);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void createTable() {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			helper.createTable(connection);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
