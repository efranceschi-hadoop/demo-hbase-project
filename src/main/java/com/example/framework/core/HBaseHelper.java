package com.example.framework.core;

import com.example.framework.annotations.*;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.compress.Compression;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.core.annotation.AnnotationConfigurationException;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Eduardo Franceschi
 * @since 2019-04-11.
 */
public class HBaseHelper<T> {

	private final Class<T> tClass;

	private HBaseTableMetaData<T> tableMetaData;

	private String tableName;
	private Method rowKeyMethod = null;

	public HBaseHelper(Class<T> tClass) {
		this.tClass = tClass;

		// Table Name Annotation
		if (tClass.isAnnotationPresent(HBaseTable.class)) {
			tableName = tClass.getAnnotation(HBaseTable.class).value();
		} else {
			throw new AnnotationConfigurationException("Can't find the annotation @" +
					HBaseTable.class.getSimpleName() +
					" on class " + tClass.getSimpleName());
		}

		// Row key Annotation
		for (Method m: tClass.getDeclaredMethods()) {
			if (m.isAnnotationPresent(HBaseRowKey.class)) {
				if (rowKeyMethod == null) {
					rowKeyMethod = m;
				} else {
					throw new AnnotationConfigurationException("Duplicated annotation @" +
							HBaseRowKey.class.getSimpleName() +
							" found on class " + tClass.getSimpleName());
				}
			}
		}
		if (rowKeyMethod == null) {
			throw new AnnotationConfigurationException("Can't find the annotation @" +
					HBaseRowKey.class.getSimpleName() +
					" on class " + tClass.getSimpleName());
		}

	}

	private void initialize() {
		if (tableMetaData != null) return;
		tableMetaData = new HBaseTableMetaData<>(tClass, null);
	}

	public byte[] extractRowKey(T obj) {
		initialize();
		try {
			Object r = rowKeyMethod.invoke(obj);
			if (r == null) return null;
			if (r instanceof String) return Bytes.toBytes((String) r);
			if (r instanceof Long) return Bytes.toBytes((Long) r);
			if (r instanceof Integer) return Bytes.toBytes((Integer) r);
			if (r instanceof Short) return Bytes.toBytes((Short) r);
			if (r instanceof Double) return Bytes.toBytes((Double) r);
			if (r instanceof Float) return Bytes.toBytes((Float) r);
			if (r instanceof Boolean) return Bytes.toBytes((Boolean) r);
			if (r instanceof BigDecimal) return Bytes.toBytes((BigDecimal) r);
			throw new UnsupportedOperationException("Can't extract rowkey from type " + r.getClass().getSimpleName());
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	public String getTableName() {
		return tableName;
	}

	public Get buildGet(byte[] key) {
		initialize();
		return new Get(key);
	}

	public Put buildPut(T obj) {
		initialize();
		byte[] rowkey = extractRowKey(obj);
		Put put = new Put(rowkey);
		tableMetaData.buildPut(put, obj);
		return put;
	}

	public T resultToObject(Result result) {
		initialize();
		T obj;
		try {
			obj = tClass.newInstance();
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
		tableMetaData.resultToObject(result, null, obj);

		return obj;
	}

	public boolean isTableExists(Connection connection) throws IOException {
		initialize();
		Admin admin = connection.getAdmin();
		return admin.tableExists(TableName.valueOf(getTableName()));
	}

	public void createTable(Connection connection) throws IOException {
		initialize();
		Admin admin = connection.getAdmin();
		HTableDescriptor tableDescriptor = new HTableDescriptor(TableName.valueOf(getTableName()));

		for (String columnFamily: tableMetaData.fieldMap.keySet()) {
			HColumnDescriptor cd = new HColumnDescriptor(columnFamily);
			cd.setCompressionType(Compression.Algorithm.SNAPPY);
			tableDescriptor.addFamily(cd);
		}

		admin.createTable(tableDescriptor);
	}

	private static class HBaseTableMetaData<T> {

		private final Field field;

		private Map<String, Map<String, Field>> fieldMap = new TreeMap<>();

		private Map<String, HBaseTableMetaData<T>> subTables = new TreeMap<>();

		@SuppressWarnings("Duplicates")
		HBaseTableMetaData(Class<T> tClass, Field field) {

			this.field = field;

			// Column & Column Families
			String defaultColumnFamily = null;

			// Default Column Family
			if (tClass.isAnnotationPresent(HBaseColumnFamily.class)) {
				defaultColumnFamily = tClass.getAnnotation(HBaseColumnFamily.class).value();
			}

			for (Field f: tClass.getDeclaredFields()) {

				// HBaseColumn
				if (f.isAnnotationPresent(HBaseColumn.class)) {
					String columnName = f.getAnnotation(HBaseColumn.class).value();

					String columnFamily;
					if (f.isAnnotationPresent(HBaseColumnFamily.class)) {
						columnFamily = f.getAnnotation(HBaseColumnFamily.class).value();
					} else {
						columnFamily = defaultColumnFamily;
					}

					if (columnFamily == null) {
						throw new AnnotationConfigurationException("Can't find an annotation @" +
								HBaseColumnFamily.class.getSimpleName() +
								" assigned to the field " + tClass.getSimpleName() + "." + f.getName());
					}

					Map<String, Field> columnList = fieldMap.get(columnFamily);
					if (columnList == null) {
						columnList = new TreeMap<>();
						if (fieldMap.put(columnFamily, columnList) != null) {
							throw new AnnotationConfigurationException("Duplicated column name found on annotation @" +
									HBaseColumnFamily.class.getSimpleName() +
									" assigned to the field " + tClass.getSimpleName() + "." + f.getName());
						}
					}
					columnList.put(columnName, f);
				}

				// HBaseCollection
				if (f.isAnnotationPresent(HBaseCollection.class)) {

					if (!Collection.class.isAssignableFrom(f.getType())) {
						throw new AnnotationConfigurationException("You need to declare a valid collection in order to use " +
								HBaseCollection.class.getSimpleName());
					}

					String columnPrefix = f.getAnnotation(HBaseCollection.class).value();

					String columnFamily;
					if (f.isAnnotationPresent(HBaseColumnFamily.class)) {
						columnFamily = f.getAnnotation(HBaseColumnFamily.class).value();
					} else {
						columnFamily = defaultColumnFamily;
					}

					if (columnFamily == null) {
						throw new AnnotationConfigurationException("Can't find an annotation @" +
								HBaseColumnFamily.class.getSimpleName() +
								" assigned to the field " + tClass.getSimpleName() + "." + f.getName());
					}

					Class<T> aClass = (Class<T>) ((ParameterizedType) f.getGenericType()).getActualTypeArguments()[0];
					subTables.put(columnPrefix, new HBaseTableMetaData<>(aClass, f));
				}
			}
		}

		public void buildPut(Put put, T obj) {
			buildPutField(put, null, obj);
			buildPutSubTables(put, obj);
		}

		private void buildPutField(Put put, String columnPrefix, T obj) {
			for (String columnFamily: fieldMap.keySet()) {
				Map<String, Field> columns = fieldMap.get(columnFamily);
				for (String column: columns.keySet()) {
					Field f = columns.get(column);
					f.setAccessible(true);
					Object value;
					try {
						value = f.get(obj);
					} catch (IllegalAccessException e) {
						throw new RuntimeException(e);
					}
					byte[] columnValue;
					if (f.getType() == String.class) columnValue = Bytes.toBytes((String) value);
					else if (f.getType() == Long.class || f.getType() == long.class) columnValue = Bytes.toBytes((Long) value);
					else if (f.getType() == Integer.class || f.getType() == int.class) columnValue = Bytes.toBytes((Integer) value);
					else if (f.getType() == Short.class || f.getType() == short.class) columnValue = Bytes.toBytes((Short) value);
					else if (f.getType() == Double.class || f.getType() == double.class) columnValue = Bytes.toBytes((Double) value);
					else if (f.getType() == Float.class || f.getType() == float.class) columnValue = Bytes.toBytes((Float) value);
					else if (f.getType() == Boolean.class || f.getType() == boolean.class) columnValue = Bytes.toBytes((Boolean) value);
					else if (f.getType() == BigDecimal.class) columnValue = Bytes.toBytes((BigDecimal) value);
					else {
						throw new UnsupportedOperationException("Can't convert field " + f.getName() +
								" from type " + f.getType().getSimpleName() + " to byte[]");
					}
					if (columnValue != null) {
						String columnName = columnPrefix == null ? column : columnPrefix + "_" + column;
						put.addColumn(columnFamily.getBytes(), columnName.getBytes(), columnValue);
					}
				}
			}
		}

		private void buildPutSubTables(Put put, T obj) {
			for (String prefix: subTables.keySet()) {
				HBaseTableMetaData metaData = subTables.get(prefix);
				try {
					metaData.field.setAccessible(true);
					Object subObj = metaData.field.get(obj);
					Collection<T> collection = (Collection<T>) subObj;
					int index = 0;
					for (T o: collection) {
						metaData.buildPutField(put, prefix + "_" + index, o);
						index++;
					}
				} catch (IllegalAccessException e) {
					throw new RuntimeException(e);
				}
			}
		}

		public boolean resultToObject(Result result, String prefix, T obj) {
			boolean found = false;
			for (String columnFamily: fieldMap.keySet()) {
				Map<String, Field> columns = fieldMap.get(columnFamily);
				for (String column : columns.keySet()) {
					String columnName = prefix == null ? column : prefix + "_" + column;
					if (result.containsColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))) {
						byte[] columnValue = result.getValue(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName));
						Field f = columns.get(column);
						Object value;
						if (f.getType() == String.class) value = Bytes.toString(columnValue);
						else if (f.getType() == Long.class || f.getType() == long.class) value = Bytes.toLong(columnValue);
						else if (f.getType() == Integer.class || f.getType() == int.class) value = Bytes.toInt(columnValue);
						else if (f.getType() == Short.class || f.getType() == short.class) value = Bytes.toShort(columnValue);
						else if (f.getType() == Double.class || f.getType() == double.class) value = Bytes.toDouble(columnValue);
						else if (f.getType() == Float.class || f.getType() == float.class) value = Bytes.toFloat(columnValue);
						else if (f.getType() == Boolean.class || f.getType() == boolean.class) value = Bytes.toBoolean(columnValue);
						else if (f.getType() == BigDecimal.class) value = Bytes.toBigDecimal(columnValue);
						else {
							throw new UnsupportedOperationException("Can't convert field " + f.getName() +
									" from type byte[] to " + f.getType().getSimpleName());
						}
						try {
							f.setAccessible(true);
							f.set(obj, value);
						} catch (IllegalAccessException e) {
							throw new RuntimeException(e);
						}
						found = true;
					}
				}
				for (String columnPrefix: subTables.keySet()) {
					HBaseTableMetaData metaData = subTables.get(columnPrefix);
					try {
						metaData.field.setAccessible(true);
						Collection<T> collection = (Collection<T>) metaData.field.get(obj);
						Class<T> aClass = (Class<T>) ((ParameterizedType) metaData.field.getGenericType()).getActualTypeArguments()[0];
						int index = 0;
						boolean success;
						do {
							try {
								T item = aClass.newInstance();
								success = metaData.resultToObject(result, columnPrefix + "_" + index, item);
								if (success) {
									found = true;
									collection.add(item);
								}
								index++;
							} catch (InstantiationException e) {
								throw new RuntimeException(e);
							}
						} while (success);
					} catch (IllegalAccessException e) {
						throw new RuntimeException(e);
					}
				}
			}
			return found;
		}
	}

}

