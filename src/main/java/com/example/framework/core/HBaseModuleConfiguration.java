package com.example.framework.core;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author Eduardo Franceschi
 * @since 2019-04-11.
 */
@ComponentScan("com.example.framework")
public class HBaseModuleConfiguration {
}
