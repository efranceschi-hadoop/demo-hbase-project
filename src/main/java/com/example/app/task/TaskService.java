package com.example.app.task;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Eduardo Franceschi
 * @since 2019-04-11.
 */
@Service
public class TaskService {

	private final TaskRepository taskRepository;

	public TaskService(TaskRepository taskRepository) {
		this.taskRepository = taskRepository;
	}

	public List<Task> findAll() {
		return taskRepository.findAll();
	}

	public List<Task> findByUsername(String username) {
		return taskRepository.findByUsername(username);
	}

	public Task findById(String username, Long taskId) {
		return taskRepository.findById(username, taskId);
	}

	public void save(Task task) {
		taskRepository.save(task);
	}

	public boolean isTableExists() {
		return taskRepository.isTableExists();
	}

	public void createTable() {
		taskRepository.createTable();
	}
}
