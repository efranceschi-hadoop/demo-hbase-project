package com.example.app.task;

import com.example.framework.annotations.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Eduardo Franceschi
 * @since 2019-04-11.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@HBaseTable("tasks")
@HBaseColumnFamily("i")
public class Task {

	@HBaseColumn("user")
	private String username;

	@HBaseColumn("id")
	private Long taskId;

	@HBaseColumn("desc")
	private String description;

	@HBaseRowKey
	public String getRowKey() {
		return username + ":" + taskId;
	}

	@Builder.Default
	@HBaseCollection("st")
	private List<SubTask> subTasks = new ArrayList<>();

}

