package com.example.app.task;

import java.util.List;

/**
 * @author Eduardo Franceschi
 * @since 2019-04-11.
 */
public interface TaskRepository {

	Task save(Task task);

	List<Task> findAll();

	List<Task> findByUsername(String username);

	Task findById(String username, Long taskId);

	boolean isTableExists();

	void createTable();

}
