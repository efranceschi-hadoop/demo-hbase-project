package com.example.app.task;

import com.example.framework.core.HBaseTemplate;
import org.apache.hadoop.conf.Configuration;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Eduardo Franceschi
 * @since 2019-04-11.
 */
@Repository
public class TaskHBaseRepository implements TaskRepository {

	private final HBaseTemplate<Task> hBaseTemplate;

	public TaskHBaseRepository(Configuration configuration) {
		this.hBaseTemplate = new HBaseTemplate<>(Task.class, configuration);
	}

	@Override
	public Task save(Task task) {
		return hBaseTemplate.save(task);
	}

	@Override
	public List<Task> findAll() {
		return hBaseTemplate.scan();
	}

	@Override
	public List<Task> findByUsername(String username) {
		return hBaseTemplate.scan(username);
	}

	@Override
	public Task findById(String username, Long taskId) {
		Task task = Task.builder()
				.username(username)
				.taskId(taskId)
				.build();
		return hBaseTemplate.findByRowKey(task);
	}

	@Override
	public boolean isTableExists() {
		return hBaseTemplate.isTableExists();
	}

	@Override
	public void createTable() {
		hBaseTemplate.createTable();
	}

}
