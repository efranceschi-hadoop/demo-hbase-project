package com.example.app.task;

import com.example.framework.annotations.HBaseColumn;
import com.example.framework.annotations.HBaseColumnFamily;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Eduardo Franceschi
 * @since 2019-04-11.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@HBaseColumnFamily("i")
public class SubTask {

	@HBaseColumn("id")
	private long subTaskId;

	@HBaseColumn("desc")
	private String description;

}
