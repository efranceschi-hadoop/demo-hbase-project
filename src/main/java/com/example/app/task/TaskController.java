package com.example.app.task;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Eduardo Franceschi
 * @since 2019-04-11.
 */
@RestController
public class TaskController {

	private final TaskService taskService;

	TaskController(TaskService taskService) {
		this.taskService = taskService;
	}

	@GetMapping(value = "/tasks/list")
	public List<Task> list() {
		return taskService.findAll();
	}

	@GetMapping(value = "/tasks/{username}")
	public List<Task> list(@PathVariable String username) {
		return taskService.findByUsername(username);
	}

	@GetMapping(value = "/tasks/{username}/{id}")
	public Task list(@PathVariable String username, @PathVariable long id) {
		return taskService.findById(username, id);
	}

}
