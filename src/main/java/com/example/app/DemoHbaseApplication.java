package com.example.app;

import com.example.app.task.SubTask;
import com.example.app.task.Task;
import com.example.app.task.TaskService;
import com.example.framework.annotations.EnableHBase;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@SpringBootApplication
@EnableHBase
public class DemoHbaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoHbaseApplication.class, args);
	}

}

@Component
class InitDatabase implements CommandLineRunner {

	private TaskService taskService;

	InitDatabase(TaskService taskService) {
		this.taskService = taskService;
	}

	@Override
	public void run(String... args) {
		if (!taskService.isTableExists()) {
			taskService.createTable();
			Stream.of("john", "mark", "carol", "alex", "michael").forEach(username -> {
				for (long taskId = 1; taskId <= 10; taskId++) {
					List<SubTask> subTasks = new ArrayList<>();
					for (long subTaskId = 1; subTaskId <= 10; subTaskId++) {
						SubTask subTask = SubTask.builder()
								.subTaskId(subTaskId)
								.description("Sub Task #" + taskId + "." + subTaskId)
								.build();
						subTasks.add(subTask);
					}
					Task task = Task.builder()
							.taskId(taskId)
							.username(username)
							.description("Task #" + taskId)
							.subTasks(subTasks)
							.build();
					taskService.save(task);
				}
			});
		}
	}

}